package com.automannn.cas.practice.casServerWebflow;

import org.apereo.cas.config.*;
import org.springframework.context.annotation.Import;
import org.springframework.stereotype.Component;

/**
 * @author chenkh
 * @time 2021/12/23 9:57
 */
@Component
@Import({
        CasCoreServicesConfiguration.class,
        CasCoreServicesAuthenticationConfiguration.class,
        CasServiceRegistryInitializationConfiguration.class
})
public class CasServerCoreAuthenticationAutoConfiguration {
}
