package com.automannn.cas.practice.casServerWebflow;

import org.apereo.cas.config.CasCoreAuthenticationServiceSelectionStrategyConfiguration;
import org.apereo.cas.config.CasCoreConfiguration;
import org.apereo.cas.config.CasCoreUtilConfiguration;
import org.apereo.cas.config.CasCoreUtilSerializationConfiguration;
import org.springframework.context.annotation.Import;
import org.springframework.stereotype.Component;

/**
 * @author chenkh
 * @time 2021/12/23 9:57
 */
@Component
@Import({
        CasCoreUtilConfiguration.class,
        CasCoreUtilSerializationConfiguration.class
})
public class CasServerCoreUtilAutoConfiguration {
}
