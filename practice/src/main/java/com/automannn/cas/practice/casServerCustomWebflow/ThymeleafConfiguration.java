package com.automannn.cas.practice.casServerCustomWebflow;

import org.springframework.context.annotation.Bean;
import org.springframework.web.context.ServletContextAware;
import org.springframework.web.servlet.ViewResolver;
import org.thymeleaf.spring5.ISpringTemplateEngine;
import org.thymeleaf.spring5.SpringTemplateEngine;
import org.thymeleaf.spring5.view.ThymeleafViewResolver;
import org.thymeleaf.templateresolver.ITemplateResolver;
import org.thymeleaf.templateresolver.ServletContextTemplateResolver;

import javax.servlet.ServletContext;
import java.nio.charset.StandardCharsets;

/**
 * @author chenkh
 * @time 2021/12/18 15:32
 */
//@Component
public class ThymeleafConfiguration implements ServletContextAware {

   private ServletContext servletContext;


    @Bean
    public ITemplateResolver automannnTemplateResolver(){
        ServletContextTemplateResolver servletContextTemplateResolver = new ServletContextTemplateResolver(this.servletContext);
        servletContextTemplateResolver.setPrefix("/templates/");
        servletContextTemplateResolver.setSuffix(".html");
        servletContextTemplateResolver.setTemplateMode("HTML5");
        servletContextTemplateResolver.setCacheable(false);
        servletContextTemplateResolver.setCharacterEncoding(StandardCharsets.UTF_8.name());
        return servletContextTemplateResolver;
    }

    @Bean
    public ISpringTemplateEngine automannnTemplateEngine(ITemplateResolver automannnTemplateResolver){
        SpringTemplateEngine springTemplateEngine = new SpringTemplateEngine();
        springTemplateEngine.setTemplateResolver(automannnTemplateResolver);
        return springTemplateEngine;
    }


    @Bean
    public ViewResolver automannnViewResolver(ISpringTemplateEngine templateEngine){
        ThymeleafViewResolver viewResolver = new ThymeleafViewResolver();
        viewResolver.setTemplateEngine(templateEngine);
        viewResolver.setCharacterEncoding(StandardCharsets.UTF_8.name());
        return viewResolver;
    }

    @Override
    public void setServletContext(ServletContext servletContext) {
        this.servletContext = servletContext;
    }
}
