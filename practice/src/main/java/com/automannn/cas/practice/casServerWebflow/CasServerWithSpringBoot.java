package com.automannn.cas.practice.casServerWebflow;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.ConfigurableApplicationContext;

/**
 * @author chenkh
 * @time 2021/12/20 11:44
 */
@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
public class CasServerWithSpringBoot {

    public static void main(String[] args) {
        SpringApplication.run(CasServerWithSpringBoot.class);
    }
}
