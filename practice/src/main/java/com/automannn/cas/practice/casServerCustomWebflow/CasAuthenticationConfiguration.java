package com.automannn.cas.practice.casServerCustomWebflow;

import org.apereo.cas.config.CasCoreConfiguration;
import org.apereo.cas.config.CasCoreServicesConfiguration;
import org.apereo.cas.config.support.CasWebApplicationServiceFactoryConfiguration;
import org.apereo.cas.web.config.CasCookieConfiguration;
import org.apereo.cas.web.config.CasSupportActionsConfiguration;
import org.springframework.context.annotation.Import;
import org.springframework.stereotype.Component;

/**
 * @author chenkh
 * @time 2021/12/18 15:47
 */
@Import({
        CasCoreServicesConfiguration.class,
        CasCookieConfiguration.class,
        CasCoreConfiguration.class,
        CasWebApplicationServiceFactoryConfiguration.class,
        CasSupportActionsConfiguration.class,
        })
@Component
public class CasAuthenticationConfiguration {
}
