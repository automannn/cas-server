package com.automannn.cas.practice.casServerWebflow;

import org.apereo.cas.config.CasCoreServicesAuthenticationConfiguration;
import org.apereo.cas.config.CasCoreServicesConfiguration;
import org.apereo.cas.config.CasServiceRegistryInitializationConfiguration;
import org.apereo.cas.config.support.CasWebApplicationServiceFactoryConfiguration;
import org.springframework.context.annotation.Import;
import org.springframework.stereotype.Component;

/**
 * @author chenkh
 * @time 2021/12/23 9:57
 */
@Component
@Import({
        CasWebApplicationServiceFactoryConfiguration.class,
        CasCoreServicesConfiguration.class,
        CasServiceRegistryInitializationConfiguration.class,
        CasCoreServicesAuthenticationConfiguration.class
})
public class CasServerCoreServicesAutoConfiguration {
}
